import java.util.regex.Pattern;
import java.util.concurrent.TimeUnit;
import org.junit.*;
import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;
import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;
import com.gargoylesoftware.htmlunit.SilentCssErrorHandler; 
import com.gargoylesoftware.htmlunit.BrowserVersion;
import com.gargoylesoftware.htmlunit.WebClient;
import java.util.logging.Level;


public class AccesPageGestionCategorie {
	private HtmlUnitDriver driver;
	private String baseUrl;
	private boolean acceptNextAlert = true;
	private StringBuffer verificationErrors = new StringBuffer();

	@Before
	public void setUp() throws Exception {
		driver = new HtmlUnitDriver(true) {
			@Override
			protected WebClient newWebClient(BrowserVersion version) {
				WebClient webClient = super.newWebClient(version);
				webClient.getOptions().setThrowExceptionOnScriptError(false);
				return webClient;
			}
		};
		baseUrl = "http://172.20.128.68";
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		java.util.logging.Logger.getLogger("com.gargoylesoftware").setLevel(Level.OFF);
		System.setProperty("org.apache.commons.logging.Log", "org.apache.commons.logging.impl.NoOpLog");
	}

	@Test
	public void testAccesPageGestionCategorie() throws Exception {
	    //driver.get(baseUrl + "/~m2test7/preprod/Source/Vendor/quinenveut/Vues/formulaire_connexion.php");
	    //Assert.assertTrue(true);
//	    driver.findElement(By.id("email")).clear();
//	    driver.findElement(By.id("email")).sendKeys("admin@admin.admin");
//	    driver.findElement(By.id("password")).clear();
//	    driver.findElement(By.id("password")).sendKeys("admin");
//	    driver.findElement(By.id("connexion")).submit();
//	    
//	    System.out.println(closeAlertAndGetItsText());
	    //driver.get(baseUrl + "/~m2test7/preprod/Source/Vendor/quinenveut/Vues/page_accueil.php");
	    //driver.findElement(By.id("admin_recap")).click();
	    //driver.findElement(By.id("admin_categories")).click();
	}

	@After
	public void tearDown() throws Exception {
		driver.quit();
		String verificationErrorString = verificationErrors.toString();
		if (!"".equals(verificationErrorString)) {
			fail(verificationErrorString);
		}
	}

	private boolean isElementPresent(By by) {
		try {
			driver.findElement(by);
			return true;
		} catch (NoSuchElementException e) {
			return false;
		}
	}

	private boolean isAlertPresent() {
		try {
			driver.switchTo().alert();
			return true;
		} catch (NoAlertPresentException e) {
			return false;
		}
	}

	private String closeAlertAndGetItsText() {
		try {
			Alert alert = driver.switchTo().alert();
			String alertText = alert.getText();
			if (acceptNextAlert) {
				alert.accept();
			} else {
				alert.dismiss();
			}
			return alertText;
		} finally {
			acceptNextAlert = true;
		}
	}
}
