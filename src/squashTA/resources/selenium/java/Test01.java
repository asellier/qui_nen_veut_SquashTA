import com.gargoylesoftware.htmlunit.BrowserVersion;
import com.gargoylesoftware.htmlunit.WebClient;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
//import org.openqa.selenium.WebDriver;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;

import java.util.concurrent.TimeUnit;
import java.util.logging.Level;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

public class Test01 {
    private HtmlUnitDriver driver;
    private String baseUrl;
    private StringBuffer verificationErrors = new StringBuffer();

    @Before
    public void setUp() throws Exception {
        driver = new HtmlUnitDriver() {
            @Override
            protected WebClient newWebClient(BrowserVersion version) {
                WebClient webClient = super.newWebClient(version);
                webClient.getOptions().setThrowExceptionOnScriptError(false);
                return webClient;
            }
        };
        baseUrl = "http://172.20.128.68/~m2test7/preprod/Vendor/quinenveut/Vues/formulaire_inscription.php/";
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        java.util.logging.Logger.getLogger("com.gargoylesoftware").setLevel(Level.OFF);
        System.setProperty("org.apache.commons.logging.Log", "org.apache.commons.logging.impl.NoOpLog");
    }

    @Test
    public void testTest01() throws Exception {

        driver.get(baseUrl);
        /*driver.findElement(By.id("nom")).clear();
        driver.findElement(By.id("nom")).sendKeys("Nom");
        driver.findElement(By.id("prenom")).clear();
        driver.findElement(By.id("prenom")).sendKeys("Prénom");
        driver.findElement(By.id("email")).clear();
        driver.findElement(By.id("email")).sendKeys("mail@mail.mail");
        driver.findElement(By.id("password")).clear();
        driver.findElement(By.id("password")).sendKeys("mdp");
        driver.findElement(By.id("passconf")).clear();
        driver.findElement(By.id("passconf")).sendKeys("mdp");
        driver.findElement(By.id("adresse")).clear();
        driver.findElement(By.id("adresse")).sendKeys("maison");
        driver.findElement(By.xpath("//button[@type='submit']")).click();*/

    }

    @After
    public void tearDown() throws Exception {
        driver.quit();
        String verificationErrorString = verificationErrors.toString();
        if (!"".equals(verificationErrorString)) {
            fail(verificationErrorString);
        }
    }
}